<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <title>Welcome To Rise</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Muhammad Rizqi Zulfian">
    <meta name="description" content="Rizqi Web">
    <meta name="keywords" content="">
    <link rel="shortcut icon" href="{{ asset('template/img/logo/logo1.png') }}">

    <!-- Bootstrap Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/css/bootstrap.min.css') }}">

    <!-- Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/plugins/owl-carousel/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/plugins/owl-carousel/owl.theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/plugins/owl-carousel/owl.transitions.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/plugins/Lightbox/dist/css/lightbox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/plugins/Icons/et-line-font/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/plugins/animate.css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/css/main.css') }}">
    <!-- Icons Font -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body>
    <!-- Preloader
	============================================= -->
    <div class="preloader"><i class="fa fa-circle-o-notch fa-spin fa-2x"></i></div>
    <!-- Header
	============================================= -->
    <section class="main-header">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="{{ asset('template/img/logo/logo.png') }}" class="img-responsive" alt="logo"></a>
                </div>
                <div class="collapse navbar-collapse text-center" id="bs-example-navbar-collapse-1">
                    <div class="col-md-8 col-xs-12 nav-wrap">
                        <ul class="nav navbar-nav">
                            <li><a href="#owl-hero" class="page-scroll">Home</a></li>
                            <li><a href="#services" class="page-scroll">Services</a></li>
                            <li><a href="#portfolio" class="page-scroll">Works</a></li>
                            <li><a href="#team" class="page-scroll">About</a></li>
                            <li><a href="#contact" class="page-scroll">Contact</a></li>
                            <!-- <li><a href="{{ url('/test123') }}" class="page-scroll">Blog</a></li> -->
                        </ul>
                    </div>
                    <div class="social-media hidden-sm hidden-xs">
                        <ul class="nav navbar-nav">
                            <li><a href="https://www.facebook.com/muhammad.rizqizulfian.1" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.dribble.com" target="_blank"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div id="owl-hero" class="owl-carousel owl-theme">
            <div class="item" style="background-image: url(../template/img/sliders/Slide.jpg)">
                <div class="caption">
                    <h6>Branding / Design / Creativity</h6>
                    <h1>We Are <span>Rise</span></h1>
                    <a class="btn btn-transparent" href="#contact">Contact Us</a><a class="btn btn-light" href="#contact">Order Now</a>
                </div>
            </div>
            <div class="item" style="background-image: url(../template/img/sliders/Slide2.jpg)">
                <div class="caption">
                    <h6>Branding / Design / Creativity</h6>
                    <h1>Creative <span>Design</span></h1>
                    <a class="btn btn-transparent" href="#contact">Contact Us</a><a class="btn btn-light" href="#contact">Order Now</a>
                </div>
            </div>
            <div class="item" style="background-image: url(../template/img/sliders/Slide3.jpg)">
                <div class="caption">
                    <h6>Branding / Design / Creativity</h6>
                    <h1>Clean <span>Code</span></h1>
                    <a class="btn btn-transparent" href="#contact">Contact Us</a><a class="btn btn-light" href="#contact">Order Now</a>
                </div>
            </div>
        </div>
    </section>

    <!-- Welcome
	============================================= -->
    <section id="welcome">
        <div class="container">
            <h2>Welcome To <span>Rise</span></h2>
            <hr class="sep">
            <p>Make Yourself At Home Don't Be Shy</p>
            <img class="img-responsive center-block wow fadeInUp" data-wow-delay=".3s" src="{{ asset('template/img/welcome/logo.png') }}" alt="logo">
        </div>
    </section>

    <!-- Services
	============================================= -->
    <section id="services">
        <div class="container">
            <h2>What We Do</h2>
            <hr class="light-sep">
            <p>We Can Do Crazy Things</p>
            <div class="services-box">
                <div class="row wow fadeInUp" data-wow-delay=".3s">
                    <div class="col-md-4">
                        <div class="media-left"><span class="icon-lightbulb"></span></div>
                        <div class="media-body">
                            <h3>Creative Design</h3>
                            <p>Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="media-left"><span class="icon-mobile"></span></div>
                        <div class="media-body">
                            <h3>Bootstrap</h3>
                            <p>Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.</p>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="media-left"><span class="icon-compass"></span></div>
                        <div class="media-body">
                            <h3>Sass <!-- &amp; --> Compass</h3>
                            <p>Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.</p>
                        </div>

                    </div>

                    <div class="row wow fadeInUp" data-wow-delay=".6s">
                        <div class="col-md-4">
                            <div class="media-left"><span class="icon-adjustments"></span></div>
                            <div class="media-body">
                                <h3>Easy To Customize</h3>
                                <p>Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.</p>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="media-left"><span class="icon-tools"></span></div>
                            <div class="media-body">
                                <h3>Photoshop</h3>
                                <p>Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.</p>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="media-left"><span class="icon-wallet"></span></div>
                            <div class="media-body">
                                <h3>Money Saver</h3>
                                <p>Fringilla augue at maximus vestibulum. Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Portfolio
	============================================= -->
    <section id="portfolio">
        <div class="container-fluid">
            <h2>Our Work</h2>
            <hr class="sep">
            <p>Showcase Your Amazing Work With Us</p>
            <div class="row">
                <div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-delay=".3s">
                    <a class="portfolio-box" href="{{ asset('template/img/portfolio/1.jpg') }}" data-lightbox="image-1" data-title="Your caption">
                        <img src="{{ asset('template/img/portfolio/1.jpg') }}" class="img-responsive" alt="1">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-delay=".3s">
                    <a href="{{ asset('template/img/portfolio/2.jpg') }}" class="portfolio-box" data-lightbox="image-2" data-title="Your caption">
                        <img src="{{ asset('template/img/portfolio/2.jpg') }}" class="img-responsive" alt="2">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-delay=".3s">
                    <a href="{{ asset('template/img/portfolio/3.jpg') }}" class="portfolio-box" data-lightbox="image-3" data-title="Your caption">
                        <img src="{{ asset('template/img/portfolio/3.jpg') }}" class="img-responsive" alt="3">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-delay=".3s">
                    <a href="{{ asset('template/img/portfolio/4.jpg') }}" class="portfolio-box" data-lightbox="image-4" data-title="Your caption">
                        <img src="{{ asset('template/img/portfolio/4.jpg') }}" class="img-responsive" alt="4">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-delay=".3s">
                    <a href="{{ asset('template/img/portfolio/5.jpg') }}" class="portfolio-box" data-lightbox="image-5" data-title="Your caption">
                        <img src="{{ asset('template/img/portfolio/5.jpg') }}" class="img-responsive" alt="5">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6 wow fadeInUp" data-wow-delay=".3s">
                    <a href="{{ asset('template/img/portfolio/6.jpg') }}" class="portfolio-box" data-lightbox="image-6" data-title="Your caption">
                        <img src="{{ asset('template/img/portfolio/6.jpg') }}" class="img-responsive" alt="6">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Category
                                </div>
                                <div class="project-name">
                                    Project Name
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- Work Process
	============================================= -->
    <section id="work-process">
        <div class="container">
            <h2>Work Process</h2>
            <hr class="sep">
            <p>What Happen In The Background</p>
            <div class="row wow fadeInUp" data-wow-delay=".3s">
                <div class="col-lg-3">
                    <span class="icon-chat"></span>
                    <h4>1.Discuss</h4>
                </div>
                <div class="col-lg-3">
                    <span class="icon-circle-compass"></span>
                    <h4>2.Sketch</h4>
                </div>
                <div class="col-lg-3">
                    <span class="icon-browser"></span>
                    <h4>3.Make</h4>
                </div>
                <div class="col-lg-3">
                    <span class="icon-global"></span>
                    <h4>4.Publish</h4>
                </div>
            </div>
        </div>
    </section>
    <!-- Some Fune Facts
	============================================= -->
    <section id="fun-facts">
        <div class="container">
            <h2>Some Fun Facts </h2>
            <hr class="light-sep">
            <p>Fun Facts</p>
            <div class="row wow fadeInUp" data-wow-delay=".3s">
                <div class="col-lg-3">
                    <span class="icon-happy"></span>
                    <h2 class="number timer">367</h2>
                    <h4>Happy Clients</h4>
                </div>
                <div class="col-lg-3">
                    <span class="icon-trophy"></span>
                    <h2 class="number timer">150</h2>
                    <h4>Project Done</h4>
                </div>
                <div class="col-lg-3">
                    <span class="icon-wine"></span>
                    <h2 class="number timer">121</h2>
                    <h4>Glass Of Wine</h4>
                </div>
                <div class="col-lg-3">
                    <span class="icon-documents"></span>
                    <h2 class="number timer">10000</h2>
                    <h4>Lines Of Code</h4>
                </div>
            </div>
        </div>
    </section>
    <!-- Some Fune Facts
	============================================= -->
    <section id="team">
        <div class="container">
            <h2>Our Team</h2>
            <hr class="sep">
            <p>Designers Behind This Work</p>
            <div class="row wow fadeInUp" data-wow-delay=".3s">
                <div class="col-md-4">
                    <div class="team">
                        <img class="img-responsive center-block" src="{{ asset('template/img/team/MariaDoe.jpg') }}" alt="1">
                        <h4>Maria Doe</h4>
                        <p>Designer</p>
                        <div class="team-social-icons">
                            <a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.dribble.com" target="_blank"><i class="fa fa-dribbble"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team">
                        <img class="img-responsive center-block" src="{{ asset('template/img/team/JasonDoe.jpg') }}" alt="2">
                        <h4>Muhammad Rizqi Zulfian</h4>
                        <p>Developer</p>
                        <div class="team-social-icons">
                            <a href="https://www.facebook.com/muhammad.rizqizulfian.1" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.dribble.com" target="_blank"><i class="fa fa-dribbble"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="team">
                        <img class="img-responsive center-block" src="{{ asset('template/img/team/MikeDoe.jpg') }}" alt="3">
                        <h4>Mike Doe</h4>
                        <p>Photographer</p>
                        <div class="team-social-icons">
                            <a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="https://www.dribble.com" target="_blank"><i class="fa fa-dribbble"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonials
	============================================= -->
    <section id="testimonials">
        <div class="container">
            <h2>Testimonials</h2>
            <hr class="light-sep">
            <p>What Clients Say About Us</p>
            <div id="owl-testi" class="owl-carousel owl-theme">
                <div class="item">
                    <div class="quote">
                        <i class="fa fa-quote-left left fa-2x"></i>
                        <h5>I’am amazed, I should say thank you so much for your awesome template. Design is so 
good and neat, every detail has been taken care these team are realy amazing and talented! I will 
work only with <span>Rise</span>.<i class="fa fa-quote-right right fa-2x"></i></h5>

                    </div>
                </div>
                <div class="item">
                    <div class="quote">
                        <i class="fa fa-quote-left left fa-2x"></i>
                        <h5>I’am amazed, I should say thank you so much for your awesome template. Design is so 
good and neat, every detail has been taken care these team are realy amazing and talented! I will 
work only with <span>Rise</span>.<i class="fa fa-quote-right right fa-2x"></i></h5>

                    </div>
                </div>
                <div class="item">
                    <div class="quote">
                        <i class="fa fa-quote-left left fa-2x"></i>
                        <h5>I’am amazed, I should say thank you so much for your awesome template. Design is so 
good and neat, every detail has been taken care these team are realy amazing and talented! I will 
work only with <span>Rise</span>.<i class="fa fa-quote-right right fa-2x"></i></h5>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Us
	============================================= -->
    <section id="contact">
        <div class="container">
            <h2>Contact Us</h2>
            <hr class="sep">
            <p>Get In Touch</p>
            <div class="col-md-6 col-md-offset-3 wow fadeInUp" data-wow-delay=".3s">
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" id="Name" placeholder="Name" name="name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="Email" placeholder="Email" name="email">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="3" placeholder="Message" name="message"></textarea>
                    </div>
                    <a href="email" class="btn-block">Send</a>
                </form>
            </div>
        </div>
    </section>
    <!-- Google Map
	============================================= -->
    <!-- <section> -->
    <!-- <div id="container" style="background-image: url(../template/img/contactus.jpg);height:35%;"> -->
    <!-- <h2>Testimonials</h2> -->
    <div id="map"></div>
    <!-- </div> -->
    <!-- </section> -->
    <!-- Footer
	============================================= -->
    <footer>
        <div class="container">
            <h1>Rise</h1>
            <div class="social">
                <a href="https://www.facebook.com/muhammad.rizqizulfian.1" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
                <a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
                <a href="https://www.dribble.com" target="_blank"><i class="fa fa-dribbble fa-2x"></i></a>
            </div>
            <h6> &copy; 2015 Rise Development By RizqiDev</h6>
        </div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="{{ asset('template/js/jquery.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="{{ asset('template/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('template/js/custom.js')}}"></script>
    <script type="text/javascript" src="{{ asset('template/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('template/js/jquery.easing.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('template/plugins/waypoints/jquery.waypoints.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('template/plugins/countTo/jquery.countTo.js')}}"></script>
    <script type="text/javascript" src="{{ asset('template/plugins/inview/jquery.inview.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('template/plugins/Lightbox/dist/js/lightbox.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('template/plugins/WOW/dist/wow.min.js')}}"></script>
    <!-- Fonts Awesome -->
    <script src="{{ asset('font-awesome/widgets.js') }}"></script>
    <!-- GOOGLE MAP -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
</body>

</html>